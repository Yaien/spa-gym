import { LOGIN, LOGOUT, READY } from "../types";

export function user(state = null, payload) {
  switch (payload.type) {
    case LOGIN:
      return payload.user;
    case LOGOUT:
      return null;
    default:
      return state;
  }
}

export function ready(state = false, payload) {
  return payload.type === READY ? true : state;
}
