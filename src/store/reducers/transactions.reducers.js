import { ADD_TRANSACTION, SET_TRANSACTIONS } from "../types";

export function transactions(state = [], payload) {
  switch (payload.type) {
    case ADD_TRANSACTION:
      return [payload.transaction, ...state];
    case SET_TRANSACTIONS:
      return payload.transactions;
    default:
      return state;
  }
}
