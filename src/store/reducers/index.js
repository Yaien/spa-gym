import { combineReducers } from "redux";

const reducers = {
  ...require("./auth.reducers"),
  ...require("./clients.reducers"),
  ...require("./plans.reducers"),
  ...require("./records.reducers"),
  ...require("./transactions.reducers"),
  ...require("./users.reducers")
};

export default combineReducers(reducers);
