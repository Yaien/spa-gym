import { ADD_RECORD, SET_RECORDS } from "../types";

export function records(state = [], payload) {
  switch (payload.type) {
    case ADD_RECORD:
      return [payload.record, ...state];
    case SET_RECORDS:
      return payload.records;
    default:
      return state;
  }
}
