import {
  ADD_CLIENT,
  SET_CLIENTS,
  UPDATE_CLIENT,
  DELETE_CLIENT
} from "../types";

export function clients(state = [], payload) {
  switch (payload.type) {
    case SET_CLIENTS:
      return payload.clients;
    case ADD_CLIENT:
      return [...state, payload.client];
    case UPDATE_CLIENT:
      let { client } = payload;
      return state.map(c => (c.id === client.id ? client : c));
    case DELETE_CLIENT:
      return state.filter(c => c.id !== payload.id);
    default:
      return state;
  }
}
