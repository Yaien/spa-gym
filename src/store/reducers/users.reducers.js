import { SET_USERS, ADD_USER, UPDATE_USER, DELETE_USER } from "../types";

export function users(state = [], payload) {
  switch (payload.type) {
    case SET_USERS:
      return payload.users;
    case ADD_USER:
      return [...state, payload.user];
    case UPDATE_USER:
      return state.map(u => (u.id === payload.user.id ? payload.user : u));
    case DELETE_USER:
      return state.filter(u => u.id !== payload.id);
    default:
      return state;
  }
}
