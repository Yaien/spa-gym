import { ADD_PLAN, SET_PLANS, UPDATE_PLAN, DELETE_PLAN } from "../types";

export function plans(state = [], payload) {
  switch (payload.type) {
    case SET_PLANS:
      return payload.plans;
    case ADD_PLAN:
      return [...state, payload.plan];
    case UPDATE_PLAN:
      let { plan } = payload;
      return state.map(c => (c.id === plan.id ? plan : c));
    case DELETE_PLAN:
      return state.filter(c => c.id !== payload.id);
    default:
      return state;
  }
}
