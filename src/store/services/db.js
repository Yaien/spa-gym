import Dexie from "dexie";
import relationships from "dexie-relationships";
import md5 from "md5";

const db = new Dexie("gym", { addons: [relationships] });

db.version(1).stores({
  users: "id++, email",
  clients: "id++, cc",
  records: "id++, clientId -> clients.id",
  transactions: "id++, clientId -> clients.id",
  plans: "id++, name",
  classes: "id++, name",
  schedule: "id++"
});

db.table("users").hook("creating", (key, user) => {
  if (!user.role) user.role = "admin";
  user.password = md5(user.password);
});

db.table("records").hook("creating", (key, record) => {
  record.date = new Date();
});

export default db;
