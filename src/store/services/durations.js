export const options = [
  { value: "h", label: "Horas" },
  { value: "d", label: "Dias" },
  { value: "w", label: "Semanas" },
  { value: "M", label: "Meses" },
  { value: "y", label: "Años" }
];

const map = new Map(options.map(opt => [opt.value, opt.label]));

export const getLabel = value => map.get(value);
