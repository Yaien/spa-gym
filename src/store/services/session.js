class Session {
  name = "SESSION";

  get id() {
    return parseInt(localStorage.getItem(this.name), 10);
  }

  set id(value) {
    return localStorage.setItem(this.name, value);
  }

  clear() {
    localStorage.removeItem(this.name);
  }
}

export default new Session();
