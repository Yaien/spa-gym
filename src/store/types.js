export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const READY = "READY";

export const ADD_CLIENT = "ADD_CLIENT";
export const UPDATE_CLIENT = "UPDATE_CLIENT";
export const DELETE_CLIENT = "DELETE_CLIENT";
export const SET_CLIENTS = "SET_CLIENTS";

export const ADD_PLAN = "ADD_PLAN";
export const UPDATE_PLAN = "UPDATE_PLAN";
export const DELETE_PLAN = "DELETE_PLAN";
export const SET_PLANS = "SET_PLANS";

export const ADD_TRANSACTION = "ADD_TRANSACTION";
export const SET_TRANSACTIONS = "SET_TRANSACTIONS";

export const ADD_USER = "ADD_USER";
export const UPDATE_USER = "UPDATE_USER";
export const DELETE_USER = "DELETE_USER";
export const SET_USERS = "SET_USERS";

export const ADD_RECORD = "ADD_RECORD";
export const SET_RECORDS = "SET_RECORDS";
