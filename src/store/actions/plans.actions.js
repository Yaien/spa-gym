import db from "../services/db";
import { SET_PLANS, ADD_PLAN, DELETE_PLAN, UPDATE_PLAN } from "../types";

const plans = db.table("plans");

export function init() {
  return async dispatch => {
    dispatch({ type: SET_PLANS, plans: await plans.toArray() });
  };
}

export function create(plan) {
  return async dispatch => {
    if (await plans.where({ name: plan.name }).count()) {
      throw new Error("nombre ya registrado");
    }
    plan.id = await plans.add(plan);
    dispatch({ type: ADD_PLAN, plan });
  };
}

export function update(plan) {
  return async dispatch => {
    let query = plans
      .where("id")
      .notEqual(plan.id)
      .and(pl => pl.name === plan.name);
    if (await query.count()) {
      throw new Error("nombre ya registrado");
    }
    await plans.update(plan.id, plan);
    dispatch({ type: UPDATE_PLAN, plan });
  };
}

export function destroy(id) {
  return async dispatch => {
    await plans.delete(id);
    dispatch({ type: DELETE_PLAN, id });
  };
}
