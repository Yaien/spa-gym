import db from "../services/db";
import { SET_USERS, ADD_USER, DELETE_USER } from "../types";

const users = db.table("users");

export function init() {
  return async dispatch => {
    dispatch({ type: SET_USERS, users: await users.toArray() });
  };
}

export function create(user) {
  return async dispatch => {
    let query = users.where("email").equals(user.email);
    if (await query.count()) {
      throw new Error("Correo ya registrado");
    }
    user.id = await users.add(user);
    dispatch({ type: ADD_USER, user });
    return user;
  };
}

export function destroy(id) {
  return async dispatch => {
    await users.delete(id);
    dispatch({ type: DELETE_USER, id });
  };
}
