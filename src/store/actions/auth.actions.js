import md5 from "md5";
import db from "../services/db";
import session from "../services/session";
import { LOGIN, LOGOUT } from "../types";
import { create } from "./users.actions";

const users = db.table("users");

export function login(email, password) {
  return async dispatch => {
    let user = await users.get({ email });
    try {
      if (!user) {
        throw new Error("Usuario no registrado");
      }
      if (user.password !== md5(password)) {
        throw new Error("Contraseña Incorrecta");
      }
    } catch (e) {
      session.clear();
      throw e;
    }
    session.id = user.id;
    dispatch({ type: LOGIN, user });
  };
}

export function register(user) {
  return async dispatch => {
    user.first = true;
    user = await dispatch(create(user));
    session.id = user.id;
    dispatch({ type: LOGIN, user });
  };
}

export function logout() {
  session.clear();
  return { type: LOGOUT };
}

export function init() {
  return async dispatch => {
    if (!session.id) return;
    let user = await users.get(session.id);
    if (!user) return session.clear();
    dispatch({ type: LOGIN, user });
  };
}
