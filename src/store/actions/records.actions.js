import db from "../services/db";
import moment from "moment";
import { ADD_RECORD, SET_RECORDS } from "../types";

const records = db.table("records");
const clients = db.table("clients");

export function create(cc) {
  return async dispatch => {
    let client = await clients.get({ cc });
    if (!client) throw new Error("Cliente no registrado");
    if (
      client.expiration &&
      moment(client.expiration).isSameOrAfter(moment(), "day")
    ) {
      let record = { clientId: client.id };
      await records.add(record);
      dispatch({ type: ADD_RECORD, record });
      return client;
    }
    throw new Error("Subscripción caducada");
  };
}

export function init() {
  return async dispatch => {
    const result = await records.with({ client: "clientId" })
    dispatch({ type: SET_RECORDS, records: result });
  };
}
