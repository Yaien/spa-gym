import { ADD_TRANSACTION, SET_TRANSACTIONS, UPDATE_CLIENT } from "../types";
import db from "../services/db";
import moment from "moment";

const transactions = db.table("transactions");
const clients = db.table("clients");

export function init() {
  return async dispatch => {
    const result = await transactions.with({ client: "clientId" })
    dispatch({ type: SET_TRANSACTIONS, transactions: result });
  };
}

export function register(client, plan, payment) {
  return async dispatch => {
    let transaction = {
      clientId: client.id,
      plan: plan.name,
      date: new Date(),
      payment
    };
    transaction.id = await transactions.add(transaction);
    dispatch({ type: ADD_TRANSACTION, transaction });
    dispatch(updateExpiration(client, plan));
  };
}

export function updateExpiration(client, plan) {
  return async dispatch => {
    // Fecha Actual
    let now = moment();

    // Castea la fecha o crea una nueva si no existe
    let exp = moment(client.expiration || undefined);

    // Verifica que la fecha de expiracion sea mas reciente a la actual
    if (exp.isBefore(now, "day")) exp = now;

    // Agrega el tiempo a la fecha de duración
    exp.add(plan.duration, plan.unit);

    // Actualiza la expiracion del cliente
    await clients.update(client.id, { expiration: exp.toDate() });

    dispatch({
      type: UPDATE_CLIENT,
      client: await clients.get(client.id)
    });
  };
}
