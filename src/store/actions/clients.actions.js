import db from "../services/db";
import {
  SET_CLIENTS,
  ADD_CLIENT,
  DELETE_CLIENT,
  UPDATE_CLIENT
} from "../types";

const clients = db.table("clients");

export function init() {
  return async dispatch => {
    dispatch({ type: SET_CLIENTS, clients: await clients.toArray() });
  };
}

export function create(client) {
  return async dispatch => {
    if (await clients.where({ cc: client.cc }).count()) {
      throw new Error("Identificacíon ya registrada");
    }
    client.id = await clients.add(client);
    dispatch({ type: ADD_CLIENT, client });
  };
}

export function update(client) {
  return async dispatch => {
    let query = clients
      .where("id")
      .notEqual(client.id)
      .and(c => c.cc === client.cc);
    if (await query.count()) {
      throw new Error("Identificacíon ya registrada");
    }
    await clients.update(client.id, client);
    dispatch({ type: UPDATE_CLIENT, client });
  };
}

export function destroy(id) {
  return async dispatch => {
    await clients.delete(id);
    dispatch({ type: DELETE_CLIENT, id });
  };
}
