import { init as auth } from "./auth.actions";
import { init as clients } from "./clients.actions";
import { init as plans } from "./plans.actions";
import { init as users } from "./users.actions";
import { init as transactions } from "./transactions.actions";
import { READY } from "../types";

export function init() {
  return async dispatch => {
    await dispatch(auth());
    await dispatch(clients());
    await dispatch(plans());
    await dispatch(users());
    await dispatch(transactions());
    dispatch({ type: READY });
  };
}
