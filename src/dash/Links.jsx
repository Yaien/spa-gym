import React from "react";
import { NavLink } from "react-router-dom";
import { NavItem } from "reactstrap";
import routes from "../routes";

export const Links = ({ user }) =>
  routes.map(({ guard, ...route }, key) => {
    return !guard || guard.match(user) ? (
      <NavItem key={key}>
        <NavLink className="nav-link" to={route.path} exact>
          {route.name}
        </NavLink>
      </NavItem>
    ) : null;
  });

export default Links;
