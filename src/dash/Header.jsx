import React from "react";
import { Navbar, NavbarBrand, NavbarToggler, Nav, Collapse } from "reactstrap";
import Links from "./Links";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Logout from "./Logout";

class Header extends React.Component {
  state = {
    isOpen: false
  };

  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  logout() {}

  render() {
    return (
      <Navbar color="light" light expand="md">
        <NavbarBrand>Gym</NavbarBrand>
        <NavbarToggler onClick={this.toggle.bind(this)} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <Links user={this.props.user} />
            {this.props.user && <Logout />}
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

const mapState = state => ({
  user: state.user
});

export default withRouter(connect(mapState)(Header));
