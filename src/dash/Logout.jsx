import React from "react";
import { connect } from "react-redux";
import { NavItem, NavLink } from "reactstrap";
import { logout } from "../store/actions/auth.actions";

const Logout = ({ logout }) => (
  <NavItem>
    <NavLink onClick={logout} style={{ cursor: "pointer" }}>
      Cerrar Session
    </NavLink>
  </NavItem>
);
const mapDispatch = dispatch => ({
  logout: () => dispatch(logout())
});

export default connect(
  null,
  mapDispatch
)(Logout);
