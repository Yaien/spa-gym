import React from "react";
import { Container } from "reactstrap";
import { Switch } from "react-router-dom";
import Route from "../routes/route";
import routes from "../routes";

const Content = () => (
  <Container fluid className="mt-3">
    <Switch>
      {routes.map((route, key) => <Route key={key} {...route} />)}
    </Switch>
  </Container>
);

export default Content;
