import React, { Fragment } from "react";
import Header from "./Header";
import Content from "./Content";

export const Dash = () => (
  <Fragment>
    <Header />
    <Content />
  </Fragment>
);

export default Dash;
