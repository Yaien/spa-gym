import { auth, guess } from "./guards";

export default [
  {
    path: "/",
    name: "Home",
    component: require("../pages/Home").default,
    exact: true,
    guard: auth
  },
  {
    path: "/login",
    name: "Inicio de Sesion",
    component: require("../pages/Login").default,
    guard: guess
  },
  {
    path: "/register",
    name: "Registro",
    component: require("../pages/Register").default,
    guard: guess
  },
  {
    path: "/clients",
    name: "Clientes",
    component: require("../pages/Clients").default,
    guard: auth
  },
  {
    path: "/plans",
    name: "Planes",
    component: require("../pages/Plans").default,
    guard: auth
  },
  {
    path: "/users",
    name: "Empleados",
    component: require("../pages/Users").default,
    guard: auth
  },
  {
    path: "/access",
    name: "Acceso",
    component: require("../pages/Access").default
  },
  {
    path: "/history",
    name: "Historial",
    component: require("../pages/History").default,
    guard: auth
  }
];
