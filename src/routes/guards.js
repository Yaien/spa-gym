export const auth = {
  match: user => !!user,
  redirect: "/login"
};

export const guess = {
  match: user => !user,
  redirect: "/"
};
