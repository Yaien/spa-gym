import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

const GuardRoute = ({ user, component: Component, guard, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        const comp = () => <Component {...props} />;
        const red = () => <Redirect to={guard.redirect} from={rest.path} />;
        if (guard) return guard.match(user) ? comp() : red();
        return comp();
      }}
    />
  );
};

const mapState = state => ({
  user: state.user
});

export default connect(mapState)(GuardRoute);
