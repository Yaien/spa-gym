import React, { Component } from "react";
import Dash from "./dash";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { init } from "./store/actions/init.actions";

class App extends Component {
  componentDidMount() {
    this.props.init();
  }

  render() {
    return this.props.ready ? <Dash /> : <div>Loading...</div>;
  }
}

const mapDispatch = dispatch => ({
  init: () => dispatch(init())
});

const mapState = state => ({
  ready: state.ready
});

export default withRouter(
  connect(
    mapState,
    mapDispatch
  )(App)
);
