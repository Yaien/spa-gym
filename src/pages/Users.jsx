import React from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
import UserTable from "../components/users/UserTable";
import UserCreate from "../components/users/UserCreate";
import UserDelete from "../components/users/UserDelete";

class Users extends React.Component {
  state = { create: false, destroy: false, selected: null };

  constructor(props) {
    super(props);
    this.openCreate = this.toggle.bind(this, "create", true, null);
    this.hideCreate = this.toggle.bind(this, "create", false, null);
    this.hideDestroy = this.toggle.bind(this, "destroy", false);
    this.openDestroy = this.toggle.bind(this, "destroy", true);
  }

  toggle(modal, open, selected) {
    this.setState({ [modal]: open, selected });
  }

  render() {
    return (
      <Row>
        <Col lg={{ size: 10, offset: 1 }}>
          <Card>
            <CardHeader>
              Empleados
              <Button
                color="primary"
                size="sm"
                className="float-right"
                onClick={this.openCreate}
              >
                AGREGAR USUARIO
              </Button>
            </CardHeader>
            <CardBody>
              <UserTable onDelete={this.openDestroy} />
            </CardBody>
          </Card>
        </Col>
        <UserCreate
          isOpen={this.state.create}
          toggle={this.hideCreate}
          onSubmitted={this.hideCreate}
        />
        {this.state.selected && (
          <UserDelete
            isOpen={this.state.destroy}
            toggle={this.hideDestroy}
            onSubmitted={this.hideDestroy}
            user={this.state.selected}
          />
        )}
      </Row>
    );
  }
}

export default Users;
