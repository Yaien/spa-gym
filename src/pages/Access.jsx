import React from "react";
import { Row, Col, Card, CardHeader, CardBody } from "reactstrap";
import AccessForm from "../components/access/AccessForm";
import ClientDetails from "../components/access/ClientDetails";

class Access extends React.Component {
  state = { lastClient: null };
  timeout = null;

  onSubmitted = client => {
    this.setState({ lastClient: client });
    this.clear();
  };

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  clear = () => {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({ lastClient: null });
    }, 30000);
  };

  render() {
    const { lastClient } = this.state;
    return (
      <Row>
        <Col lg={{ size: 10, offset: 1 }}>
          <Card>
            <CardHeader>Acceso Clientes</CardHeader>
            <CardBody>
              <Row>
                <Col md={6}>
                  <AccessForm onSubmitted={this.onSubmitted} />
                </Col>
                <Col md={6}>
                  {lastClient && <ClientDetails client={lastClient} />}
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default Access;
