import React from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";
import LoginForm from "../components/auth/LoginForm";

const Login = () => (
  <Row>
    <Col lg={{ size: 8, offset: 2 }}>
      <Card>
        <CardHeader>Inicio De Sesión</CardHeader>
        <CardBody>
          <LoginForm />
        </CardBody>
      </Card>
    </Col>
  </Row>
);

export default Login;
