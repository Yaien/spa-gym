import React, { Component, Fragment } from "react";
import { Card, CardHeader, CardBody, Button, Row, Col } from "reactstrap";
import PlanTable from "../components/plans/PlanTable";
import PlanDelete from "../components/plans/PlanDelete";
import PlanCreate from "../components/plans/PlanCreate";
import PlanEdit from "../components/plans/PlanEdit";

class Plans extends Component {
  state = { edit: false, create: false, destroy: false, selected: null };

  constructor(props) {
    super(props);
    this.openCreate = this.toggle.bind(this, "create", true, null);
    this.hideCreate = this.toggle.bind(this, "create", false, null);
    this.openEdit = this.toggle.bind(this, "edit", true);
    this.hideEdit = this.toggle.bind(this, "edit", false);
    this.openDestroy = this.toggle.bind(this, "destroy", true);
    this.hideDestroy = this.toggle.bind(this, "destroy", false);
  }

  toggle(modal, open, selected) {
    this.setState({ [modal]: open, selected });
  }

  render() {
    const { edit, create, destroy, selected } = this.state;
    return (
      <Row>
        <Col sm={{ size: 10, offset: 1 }}>
          <Card>
            <CardHeader>
              <h5 className="d-inline">Planes</h5>
              <Button
                size="sm"
                color="primary"
                className="float-right"
                onClick={this.openCreate}
              >
                AGREGAR PLAN
              </Button>
            </CardHeader>
            <CardBody>
              <PlanTable onEdit={this.openEdit} onDelete={this.openDestroy} />
            </CardBody>
            <PlanCreate
              isOpen={create}
              toggle={this.hideCreate}
              onSubmitted={this.hideCreate}
            />
            {selected && (
              <Fragment>
                <PlanEdit
                  isOpen={edit}
                  toggle={this.hideCreate}
                  onSubmitted={this.hideEdit}
                  plan={selected}
                />
                <PlanDelete
                  isOpen={destroy}
                  toggle={this.hideDestroy}
                  onSubmitted={this.hideDestroy}
                  plan={selected}
                />
              </Fragment>
            )}
          </Card>
        </Col>
      </Row>
    );
  }
}

export default Plans;
