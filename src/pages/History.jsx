import React from "react";
import { Card, CardHeader, CardBody, Row, Col, ButtonGroup } from "reactstrap";
import { Route, Switch, NavLink, Redirect } from "react-router-dom";
import Sales from "../components/history/Sales";
import Logs from "../components/history/Logs";

const NavLinkBtn = ({ children, ...props }) => (
  <NavLink className="btn btn-info" {...props}>
    {children}
  </NavLink>
);

class History extends React.Component {
  render() {
    return (
      <Row>
        <Col lg={{ size: 10, offset: 1 }}>
          <Card>
            <CardHeader>
              Historial
              <ButtonGroup size="sm" className="float-right">
                <NavLinkBtn to="/history/ventas">VENTAS</NavLinkBtn>
                <NavLinkBtn to="/history/logs">REGISTRO DE ENTRADA</NavLinkBtn>
              </ButtonGroup>
            </CardHeader>
            <CardBody>
              <Switch>
                <Route path="/history/ventas" component={Sales} />
                <Route path="/history/logs" component={Logs} />
                <Redirect from="/history" to="/history/ventas" exact />
              </Switch>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default History;
