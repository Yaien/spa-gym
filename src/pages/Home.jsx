import React from "react";
import { Row, Col } from "reactstrap";
import DayCard from "../components/stadistics/DayCard";
import MonthCard from "../components/stadistics/MonthCard";
import ClientCard from "../components/stadistics/ClientCard";

const Home = () => (
  <Row>
    <Col md={4}>
      <DayCard />
    </Col>
    <Col md={4}>
      <MonthCard />
    </Col>
    <Col md={4}>
      <ClientCard />
    </Col>
  </Row>
);

export default Home;
