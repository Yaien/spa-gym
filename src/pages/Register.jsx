import React from "react";
import { Col, Row, Card, CardHeader, CardBody } from "reactstrap";
import RegisterForm from "../components/auth/RegisterForm";

const Register = () => (
  <Row>
    <Col lg={{ size: 8, offset: 2 }}>
      <Card>
        <CardHeader>Registro</CardHeader>
        <CardBody>
          <RegisterForm />
        </CardBody>
      </Card>
    </Col>
  </Row>
);

export default Register;
