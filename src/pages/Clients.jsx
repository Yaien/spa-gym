import React from "react";
import { Card, CardHeader, CardBody, Row, Col, Button } from "reactstrap";
import ClientTable from "../components/clients/ClientTable";
import Transaction from "../components/clients/Transaction";
import Edit from "../components/clients/Edit";
import Create from "../components/clients/Create";

class Clients extends React.Component {
  state = {
    modals: { create: false, edit: false, subs: false },
    selected: null
  };

  constructor(props) {
    super(props);
    this.openCreate = this.toggle.bind(this, "create", true, null);
    this.openEdit = this.toggle.bind(this, "edit", true);
    this.openTransaction = this.toggle.bind(this, "subs", true);
    this.hideCreate = this.hide.bind(this, "create");
    this.hideTransaction = this.hide.bind(this, "subs");
    this.hideEdit = this.hide.bind(this, "edit");
  }

  /**
   * Toggle Modal
   * @param {string} modal
   * @param {boolean} open
   */
  toggle(modal, open, selected) {
    this.setState({
      modals: { ...this.state.modals, [modal]: open },
      selected
    });
  }

  hide(modal) {
    this.setState({
      modals: { ...this.state.modals, [modal]: false }
    });
  }

  render() {
    const { modals, selected } = this.state;
    return (
      <Row>
        <Col lg={{ size: 10, offset: 1 }}>
          <Card>
            <CardHeader>
              Clientes
              <Button
                size="sm"
                color="primary"
                className="float-right"
                onClick={this.openCreate}
              >
                AGREGAR CLIENTE
              </Button>
            </CardHeader>
            <CardBody>
              <ClientTable
                onEdit={this.openEdit}
                onSubscription={this.openTransaction}
              />
            </CardBody>
          </Card>
          <Create
            isOpen={modals.create}
            toggle={this.hideCreate}
            onSubmitted={this.hideCreate}
          />

          <Edit
            isOpen={modals.edit}
            toggle={this.hideEdit}
            onSubmitted={this.hideEdit}
            client={selected}
          />
          <Transaction
            client={selected}
            isOpen={modals.subs}
            toggle={this.hideTransaction}
            onSubmitted={this.hideTransaction}
          />
        </Col>
      </Row>
    );
  }
}

export default Clients;
