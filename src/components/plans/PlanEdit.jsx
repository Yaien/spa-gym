import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import PlanForm from "./PlanForm";

const PlanEdit = ({ isOpen, toggle, plan, onSubmitted }) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>Editar Plan</ModalHeader>
    <ModalBody>
      <PlanForm plan={plan} mode="update" onSubmitted={onSubmitted} />
    </ModalBody>
  </Modal>
);

export default PlanEdit;
