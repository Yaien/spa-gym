import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import PlanForm from "./PlanForm";

const PlanCreate = ({ isOpen, toggle, onSubmitted }) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>Agregar Plan</ModalHeader>
    <ModalBody>
      <PlanForm onSubmitted={onSubmitted} />
    </ModalBody>
  </Modal>
);

export default PlanCreate;
