import React from "react";
import { Modal, ModalHeader, ModalBody, Button } from "reactstrap";
import { destroy } from "../../store/actions/plans.actions";
import { connect } from "react-redux";

class PlanDelete extends React.PureComponent {
  async destroy() {
    const { onSubmitted, plan } = this.props;
    await this.props.destroy(plan.id);
    if (onSubmitted) onSubmitted(plan);
  }

  render() {
    const { isOpen, toggle, plan } = this.props;
    return (
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Eliminar Plan</ModalHeader>
        <ModalBody className="text-center">
          <p>Esta seguro de eliminar a {plan.name}?</p>
          <Button color="primary" onClick={this.destroy.bind(this)}>
            Eliminar
          </Button>
        </ModalBody>
      </Modal>
    );
  }
}

const mapDispatch = dispatch => ({
  destroy: id => dispatch(destroy(id))
});

export default connect(
  null,
  mapDispatch
)(PlanDelete);
