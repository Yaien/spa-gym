import React from "react";
import FormInput from "../inputs/FormInput";
import { Alert, Button, Row, Col } from "reactstrap";
import { connect } from "react-redux";
import { create, update } from "../../store/actions/plans.actions";
import { options } from "../../store/services/durations";

class PlanForm extends React.Component {
  state = { plan: {}, status: null };

  componentDidMount() {
    this.setState({ plan: { ...this.props.plan } });
  }

  change = ({ target }) => {
    let value =
      target.type === "number" ? parseInt(target.value, 10) : target.value;
    this.setState({
      plan: { ...this.state.plan, [target.name]: value }
    });
  };

  modes = {
    create: {
      submitText: "REGISTRAR",
      submit: this.props.create
    },
    update: {
      submitText: "ACTUALIZAR",
      submit: this.props.update
    }
  };

  submit = async (call, e) => {
    e.preventDefault();
    try {
      await call(this.state.plan);
      if (this.props.onSubmitted) this.props.onSubmitted(this.state.plan);
    } catch (e) {
      this.setState({ status: e.message });
    }
  };

  render() {
    const { plan } = this.state;
    const mode = this.modes[this.props.mode] || this.modes.create;
    return (
      <form onSubmit={this.submit.bind(this, mode.submit)}>
        {this.state.status && <Alert color="danger">{this.state.status}</Alert>}
        <FormInput
          name="name"
          label="Nombre"
          required
          value={plan.name || ""}
          onChange={this.change}
        />
        <Row>
          <Col>
            <FormInput
              type="number"
              name="duration"
              label="Duracion"
              required
              min="1"
              value={plan.duration || ""}
              onChange={this.change}
            />
          </Col>
          <Col>
            <FormInput
              type="select"
              label="Unidad"
              required
              name="unit"
              onChange={this.change}
            >
              {options.map((d, i) => (
                <option key={i} value={d.value}>
                  {d.label}
                </option>
              ))}
            </FormInput>
          </Col>
        </Row>
        <FormInput
          name="price"
          type="number"
          label="Precio"
          min="0"
          required
          value={plan.price || ""}
          onChange={this.change}
        />
        <Button type="submit" color="primary" className="m-auto d-block">
          {mode.submitText}
        </Button>
      </form>
    );
  }
}

const mapDispatch = dispatch => ({
  create: plan => dispatch(create(plan)),
  update: plan => dispatch(update(plan))
});

export default connect(
  null,
  mapDispatch
)(PlanForm);
