import React from "react";
import { Table, Button, ButtonGroup } from "reactstrap";
import { connect } from "react-redux";
import { getLabel } from "../../store/services/durations";

const PlanTable = ({ plans, onEdit, onDelete }) => (
  <Table responsive bordered className="text-center">
    <thead className="thead-light">
      <tr>
        <th>Nombre</th>
        <th>Duración</th>
        <th>Precio</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      {plans.map(plan => (
        <tr key={plan.id}>
          <td>{plan.name}</td>
          <td>
            {plan.duration} {getLabel(plan.unit)}
          </td>

          <td>${plan.price}</td>
          <td>
            <ButtonGroup size="sm">
              <Button
                title="Editar"
                color="warning"
                onClick={onEdit && onEdit.bind(null, plan)}
              >
                <span className="fa fa-edit" />
              </Button>
              <Button
                title="Eliminar"
                color="danger"
                onClick={onDelete && onDelete.bind(null, plan)}
              >
                <span className="fa fa-remove" />
              </Button>
            </ButtonGroup>
          </td>
        </tr>
      ))}
    </tbody>
  </Table>
);

const mapState = state => ({
  plans: state.plans
});

export default connect(mapState)(PlanTable);
