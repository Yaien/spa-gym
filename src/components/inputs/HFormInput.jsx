import React from "react";
import { FormGroup, Label, Col, Input } from "reactstrap";

const HFormInput = ({ label, children, ...props }) => (
  <FormGroup row>
    <Label md={3}>{label}</Label>
    <Col md={9}>
      <Input {...props}>{children}</Input>
    </Col>
  </FormGroup>
);

export default HFormInput;
