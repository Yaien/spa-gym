import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

const FormInput = ({ label, children, ...props }) => (
  <FormGroup>
    <Label>{label}</Label>
    <Input {...props}>{children}</Input>
  </FormGroup>
);

export default FormInput;
