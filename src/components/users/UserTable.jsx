import React from "react";
import { Table, ButtonGroup, Button } from "reactstrap";
import { connect } from "react-redux";

const FirstRow = ({ user }) => (
  <tr className="table-warning">
    <td>{user.name}</td>
    <td>{user.email}</td>
    <td>{user.role}</td>
    <td />
  </tr>
);

const Row = ({ user, onDelete }) => (
  <tr>
    <td>{user.name}</td>
    <td>{user.email}</td>
    <td>{user.role}</td>
    <td>
      <ButtonGroup size="sm">
        <Button
          color="danger"
          title="Eliminar"
          onClick={onDelete && onDelete.bind(null, user)}
        >
          <span className="fa fa-remove" />
        </Button>
      </ButtonGroup>
    </td>
  </tr>
);

const UserTable = ({ users, onDelete }) => (
  <Table bordered responsive className="text-center">
    <thead className="thead-light">
      <tr>
        <th>Nombre</th>
        <th>Correo</th>
        <th>Rol</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      {users.map(
        user =>
          user.first ? (
            <FirstRow key={user.id} user={user} />
          ) : (
            <Row key={user.id} user={user} onDelete={onDelete} />
          )
      )}
    </tbody>
  </Table>
);

const mapState = state => ({
  users: state.users
});

export default connect(mapState)(UserTable);
