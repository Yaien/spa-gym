import React from "react";
import { Form, Button, Alert } from "reactstrap";
import { connect } from "react-redux";
import { create } from "../../store/actions/users.actions";
import HFormInput from "../inputs/HFormInput";

class UserForm extends React.Component {
  state = { user: {}, status: null };

  change = ({ target }) => {
    this.setState({
      user: { ...this.state.user, [target.name]: target.value }
    });
  };

  submit = async e => {
    e.preventDefault();
    try {
      await this.props.create(this.state.user);
      if (this.props.onSubmitted) this.props.onSubmitted();
    } catch (e) {
      this.setState({ status: e.message });
    }
  };

  render() {
    return (
      <Form onSubmit={this.submit}>
        {this.state.status && <Alert color="danger">{this.state.status}</Alert>}
        <HFormInput
          name="name"
          label="Nombre"
          required
          pattern="^[\w\s]+"
          onChange={this.change}
        />
        <HFormInput
          name="email"
          type="email"
          label="Correo"
          required
          onChange={this.change}
        />
        <HFormInput
          name="password"
          type="password"
          label="Contraseña"
          required
          onChange={this.change}
        />
        <HFormInput
          name="confirm"
          type="password"
          label="Confirmar"
          required="required"
          title="Las contraseñas no coinciden"
          pattern={`^${this.state.user.password || ""}$`}
        />
        <HFormInput
          type="select"
          required
          label="Cargo"
          name="role"
          onChange={this.change}
        >
          <option value="" />
          <option value="admin">Admin</option>
          <option value="empleado">Empleado</option>
        </HFormInput>
        <Button className="m-auto d-block" color="primary">
          REGISTRAR
        </Button>
      </Form>
    );
  }
}

const mapDispatch = dispatch => ({
  create: user => dispatch(create(user))
});

export default connect(
  null,
  mapDispatch
)(UserForm);
