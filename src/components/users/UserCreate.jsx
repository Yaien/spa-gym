import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import UserForm from "./UserForm";

const UserCreate = ({ isOpen, toggle, onSubmitted }) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>Agregar Empleado</ModalHeader>
    <ModalBody>
      <UserForm onSubmitted={onSubmitted} />
    </ModalBody>
  </Modal>
);

export default UserCreate;
