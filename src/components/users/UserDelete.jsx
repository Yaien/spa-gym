import React from "react";
import { Modal, ModalHeader, ModalBody, Button } from "reactstrap";
import { connect } from "react-redux";
import { destroy } from "../../store/actions/users.actions";

class UserDelete extends React.PureComponent {
  async destroy() {
    const { onSubmitted, user } = this.props;
    await this.props.destroy(user.id);
    if (onSubmitted) onSubmitted(user);
  }

  render() {
    const { isOpen, toggle, user } = this.props;
    return (
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Eliminar Empleado</ModalHeader>
        <ModalBody className="text-center">
          <p>Esta seguro de eliminar a {user.name}?</p>
          <Button color="primary" onClick={this.destroy.bind(this)}>
            Eliminar
          </Button>
        </ModalBody>
      </Modal>
    );
  }
}

const mapDispatch = dispatch => ({
  destroy: id => dispatch(destroy(id))
});

export default connect(
  null,
  mapDispatch
)(UserDelete);
