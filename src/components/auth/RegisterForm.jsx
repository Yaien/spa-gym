import React, { Component } from "react";
import HFormInput from "../inputs/HFormInput";
import { Button, Alert } from "reactstrap";
import { register } from "../../store/actions/auth.actions";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class RegisterForm extends Component {
  state = { user: {}, status: null };

  change = ({ target }) => {
    this.setState({
      user: { ...this.state.user, [target.name]: target.value }
    });
  };

  submit = async e => {
    e.preventDefault();
    try {
      await this.props.register(this.state.user);
      this.props.history.push("/");
    } catch (e) {
      this.setState({ status: e.message });
    }
  };

  render() {
    return (
      <form onSubmit={this.submit}>
        {this.state.status && <Alert color="danger">{this.state.status}</Alert>}
        <HFormInput
          name="name"
          label="Nombre"
          required
          pattern="^[\w\s]+"
          onChange={this.change}
        />
        <HFormInput
          name="email"
          type="email"
          label="Correo"
          required="required"
          onChange={this.change}
        />
        <HFormInput
          name="password"
          type="password"
          label="Contraseña"
          required="required"
          onChange={this.change}
        />
        <HFormInput
          name="confirm"
          type="password"
          label="Confirmar Contraseña"
          required="required"
          title="Las contraseñas no coinciden"
          pattern={`^${this.state.user.password || ""}$`}
        />
        <Button className="m-auto d-block" color="primary">
          REGISTRARSE
        </Button>
      </form>
    );
  }
}

const mapDispatch = dispatch => ({
  register: user => dispatch(register(user))
});

export default withRouter(
  connect(
    null,
    mapDispatch
  )(RegisterForm)
);
