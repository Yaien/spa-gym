import React, { Component } from "react";
import { Form, Button, Alert } from "reactstrap";
import HFormInput from "../inputs/HFormInput";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { login } from "../../store/actions/auth.actions";

class LoginForm extends Component {
  state = {
    login: {},
    status: null
  };

  change = ({ target }) => {
    this.setState({
      login: { ...this.state.login, [target.name]: target.value }
    });
  };

  submit = async e => {
    e.preventDefault();
    try {
      let { username, password } = this.state.login;
      await this.props.login(username, password);
      this.props.history.push("/");
    } catch (e) {
      this.setState({ status: e.message });
    }
  };

  render() {
    return (
      <Form onSubmit={this.submit}>
        {this.state.status && <Alert color="danger">{this.state.status}</Alert>}
        <HFormInput
          label="Correo"
          type="email"
          name="username"
          required
          onChange={this.change}
        />
        <HFormInput
          label="Contraseña"
          type="password"
          name="password"
          required
          onChange={this.change}
        />
        <Button type="submit" color="primary" className="m-auto d-block">
          Iniciar Sesion
        </Button>
      </Form>
    );
  }
}

const mapDispatch = dispatch => ({
  login: (username, password) => dispatch(login(username, password))
});

export default withRouter(
  connect(
    null,
    mapDispatch
  )(LoginForm)
);
