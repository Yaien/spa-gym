import React from "react";
import { connect } from "react-redux";
import CardStat from "./CardStat";
import moment from "moment";

const DayCard = ({ value }) => (
  <CardStat icon="money" color="primary" inverse>
    <b>${value}</b> en ventas del dia
  </CardStat>
);

const mapState = ({ transactions }) => {
  const now = moment();
  let value = transactions
    .filter(tr => moment(tr.date).isSame(now, "day"))
    .reduce((sum, tr) => sum + tr.payment, 0);
  return { value };
};

export default connect(mapState)(DayCard);
