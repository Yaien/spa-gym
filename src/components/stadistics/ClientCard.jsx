import React from "react";
import { connect } from "react-redux";
import CardStat from "./CardStat";

const ClientCard = ({ value }) => (
  <CardStat icon="users" color="success" inverse>
    <b>{value}</b> clientes registrados
  </CardStat>
);

const mapState = ({ clients }) => {
  return { value: clients.length };
};

export default connect(mapState)(ClientCard);
