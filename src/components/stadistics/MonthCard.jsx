import React from "react";
import { connect } from "react-redux";
import CardStat from "./CardStat";
import moment from "moment";

const MonthCard = ({ value }) => (
  <CardStat icon="bank" color="danger" inverse>
    <b>${value}</b> en ventas del mes
  </CardStat>
);

const mapState = ({ transactions }) => {
  const now = moment();
  let value = transactions
    .filter(tr => moment(tr.date).isSame(now, "month"))
    .reduce((sum, tr) => sum + tr.payment, 0);
  return { value };
};

export default connect(mapState)(MonthCard);
