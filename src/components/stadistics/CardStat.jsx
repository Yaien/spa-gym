import React from "react";
import { Card } from "reactstrap";
import styled from "styled-components";

const CardBodyIcon = styled.div`
  position: absolute;
  top: -25px;
  right: 0px;
  font-size: 5rem;
  transform: rotate(20deg);
}
`;

const CardStat = ({ icon, children, ...props }) => (
  <Card body style={{ overflow: "hidden" }} {...props}>
    <CardBodyIcon>
      <i className={"fa fa-" + icon} />
    </CardBodyIcon>
    <div className="mr-5">{children}</div>
  </Card>
);

export default CardStat;
