import React from "react";
import { Table, ButtonGroup, Button } from "reactstrap";
import { connect } from "react-redux";
import moment from "moment";

const ClientTable = ({ clients, onEdit, onSubscription }) => (
  <Table responsive bordered className="text-center">
    <thead className="thead-light">
      <tr>
        <th>ID</th>
        <th>nombre</th>
        <th>correo</th>
        <th>teléfono</th>
        <th>expiración</th>
        <th>acciones</th>
      </tr>
    </thead>
    <tbody>
      {clients.map(client => (
        <tr key={client.id}>
          <td>{client.cc}</td>
          <td>{client.name}</td>
          <td>{client.email}</td>
          <td>{client.phone}</td>
          <td>
            {client.expiration ? moment(client.expiration).format("LL") : ""}
          </td>
          <td>
            <ButtonGroup size="sm">
              <Button
                color="warning"
                title="Editar"
                onClick={onEdit && onEdit.bind(null, client)}
              >
                <span className="fa fa-edit" />
              </Button>
              {onSubscription && (
                <Button
                  color="primary"
                  title="Subscripción"
                  onClick={onSubscription.bind(null, client)}
                >
                  <span className="fa fa-user" />
                </Button>
              )}
            </ButtonGroup>
          </td>
        </tr>
      ))}
    </tbody>
  </Table>
);

const mapState = state => ({
  clients: state.clients
});

export default connect(mapState)(ClientTable);
