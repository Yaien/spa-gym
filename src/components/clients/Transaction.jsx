import React from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import TransactionForm from "./TransactionForm";

const Transaction = ({ isOpen, toggle, client, onSubmitted }) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>
      Subscripcion
      {client && <span className="d-block text-secondary">{client.name}</span>}
    </ModalHeader>
    <ModalBody>
      <TransactionForm client={client} onSubmitted={onSubmitted} />
    </ModalBody>
  </Modal>
);

export default Transaction;
