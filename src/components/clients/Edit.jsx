import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import ClientForm from "./ClientForm";

export const Edit = ({ isOpen, toggle, client, onSubmitted }) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>Editar Cliente</ModalHeader>
    <ModalBody>
      <ClientForm mode="update" client={client} onSubmitted={onSubmitted} />
    </ModalBody>
  </Modal>
);

export default Edit;
