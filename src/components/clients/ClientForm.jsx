import React from "react";
import HFormInput from "../inputs/HFormInput";
import { Alert, Button } from "reactstrap";
import { connect } from "react-redux";
import { create, update } from "../../store/actions/clients.actions";

class ClientForm extends React.Component {
  state = { client: {}, status: null };

  componentDidMount() {
    this.setState({ client: { ...this.props.client } });
  }

  change = ({ target }) => {
    this.setState({
      client: { ...this.state.client, [target.name]: target.value }
    });
  };

  modes = {
    create: {
      submitText: "REGISTRAR",
      submit: this.props.create
    },
    update: {
      submitText: "ACTUALIZAR",
      submit: this.props.update
    }
  };

  submit = async (call, e) => {
    e.preventDefault();
    try {
      await call(this.state.client);
      if (this.props.onSubmitted) this.props.onSubmitted();
    } catch (e) {
      this.setState({ status: e.message });
    }
  };

  render() {
    const { client } = this.state;
    const mode = this.modes[this.props.mode] || this.modes.create;
    return (
      <form onSubmit={this.submit.bind(this, mode.submit)}>
        {this.state.status && <Alert color="danger">{this.state.status}</Alert>}
        <HFormInput
          name="cc"
          label="ID"
          required
          pattern="^[\d]+$"
          value={client.cc || ""}
          onChange={this.change}
        />
        <HFormInput
          name="name"
          label="Nombre"
          required
          value={client.name || ""}
          onChange={this.change}
        />
        <HFormInput
          type="email"
          name="email"
          label="Correo"
          required
          value={client.email || ""}
          onChange={this.change}
        />
        <HFormInput
          name="phone"
          label="Telefono"
          required
          value={client.phone || ""}
          onChange={this.change}
        />
        <Button type="submit" color="primary" className="m-auto d-block">
          {mode.submitText}
        </Button>
      </form>
    );
  }
}

const mapDispatch = dispatch => ({
  create: client => dispatch(create(client)),
  update: client => dispatch(update(client))
});

export default connect(
  null,
  mapDispatch
)(ClientForm);
