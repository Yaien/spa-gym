import React, { Component } from "react";
import { connect } from "react-redux";
import HFormInput from "../inputs/HFormInput";
import { Form, Button } from "reactstrap";
import { getLabel } from "../../store/services/durations";
import { register } from "../../store/actions/transactions.actions";

class TransactionForm extends Component {
  state = {
    transaction: {},
    status: null
  };

  changePlan = ({ target }) => {
    let planId = Number(target.value);
    let plan = this.props.plans.find(p => p.id === planId);
    this.setState({
      transaction: { plan, payment: plan.price }
    });
  };

  change(isNumber = false, { target }) {
    let { name, value } = target;
    if (isNumber) value = Number(value);
    this.setState({
      transaction: { ...this.state.transaction, [name]: value }
    });
  }

  submit = async e => {
    e.preventDefault();
    const { plan, payment } = this.state.transaction;
    try {
      await this.props.register(this.props.client, plan, payment);
      if (this.props.onSubmitted) this.props.onSubmitted();
    } catch (e) {
      this.setState({ status: e.message });
    }
  };

  render() {
    return (
      <Form onSubmit={this.submit}>
        <HFormInput
          type="select"
          label="Plan"
          name="plan"
          required
          onChange={this.changePlan}
        >
          <option value="" />
          {this.props.plans.map(plan => (
            <option key={plan.id} value={plan.id}>
              {plan.name} ({plan.duration} {getLabel(plan.unit)} - ${plan.price})
            </option>
          ))}
        </HFormInput>
        <HFormInput
          type="number"
          label="Pago"
          name="payment"
          required
          value={this.state.transaction.payment || ""}
          onChange={this.change.bind(this, true)}
        />
        <Button type="submit" color="primary" className="m-auto d-block">
          Registrar Pago
        </Button>
      </Form>
    );
  }
}

const mapState = state => ({
  plans: state.plans
});

const mapDispatch = dispatch => ({
  register: (client, plan, payment) => dispatch(register(client, plan, payment))
});

export default connect(
  mapState,
  mapDispatch
)(TransactionForm);
