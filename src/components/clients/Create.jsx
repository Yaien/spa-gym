import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import ClientForm from "./ClientForm";

export const Create = ({ isOpen, toggle, onSubmitted }) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>Agregar Cliente</ModalHeader>
    <ModalBody>
      <ClientForm onSubmitted={onSubmitted} />
    </ModalBody>
  </Modal>
);

export default Create;
