import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Table } from "reactstrap";
import moment from "moment";
import { init } from "../../store/actions/transactions.actions";

class Sales extends React.Component {
  constructor(props) {
    super(props)
    this.state = { ready: false }
  }
  async componentDidMount() {
    await this.props.init();
    this.setState({ ready: true })
  }
  render() {
    if (!this.state.ready) {
      return null;
    }
    return (
      <Table size="sm" responsive bordered className="text-center">
        <thead className="thead-light">
          <tr>
            <th>Plan</th>
            <th>Cliente</th>
            <th>Pago</th>
            <th>Fecha y Hora</th>
          </tr>
        </thead>
        <tbody>
          {this.props.transactions.map(tr => (
            <tr key={tr.id}>
              <td>{tr.plan}</td>
              <td>{tr.client.name}</td>
              <td>${tr.payment}</td>
              <td>{moment(tr.data).format("LLL")}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

const mapState = state => ({
  transactions: state.transactions
});

const mapDispatch = dispatch => ({
  init: () => dispatch(init())
});

export default withRouter(
  connect(
    mapState,
    mapDispatch
  )(Sales)
);
