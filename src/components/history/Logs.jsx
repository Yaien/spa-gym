import React from "react";
import { Table } from "reactstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import moment from "moment";
import { init } from "../../store/actions/records.actions";

class Logs extends React.Component {
  constructor(props) {
    super(props)
    this.state = { ready: false }
  }

  async componentDidMount() {
    await this.props.init();
    this.setState({ ready: true })
  }
  render() {
    if (!this.state.ready) {
      return null
    }
    return (
      <Table size="sm" responsive bordered className="text-center">
        <thead className="thead-light">
          <tr>
            <th>Cliente</th>
            <th>Fecha y Hora</th>
          </tr>
        </thead>
        <tbody>
          {this.props.records.map(rec => (
            <tr key={rec.id}>
              <td>{rec.client.name}</td>
              <td>{moment(rec.date).format("LLL")}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}

const mapState = state => ({
  records: state.records
});

const mapDispatch = dispath => ({
  init: () => dispath(init())
});

export default withRouter(
  connect(
    mapState,
    mapDispatch
  )(Logs)
);
