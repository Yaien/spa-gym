import React from "react";
import { Table } from "reactstrap";
import moment from "moment";

const ClientDetails = ({ client }) => (
  <Table striped responsive size="sm">
    <tbody>
      <tr>
        <th>Cedula</th>
        <td>{client.cc}</td>
      </tr>
      <tr>
        <th>Nombre</th>
        <td>{client.name}</td>
      </tr>
      <tr>
        <th>Correo</th>
        <td>{client.email}</td>
      </tr>
      <tr>
        <th>Telefono</th>
        <td>{client.phone}</td>
      </tr>
      <tr>
        <th>Expiracion</th>
        <td>{moment(client.expiration).format("LL")}</td>
      </tr>
    </tbody>
  </Table>
);

export default ClientDetails;
