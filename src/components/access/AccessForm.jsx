import React from "react";
import { connect } from "react-redux";
import { create } from "../../store/actions/records.actions";
import { Form, Button, Alert, FormGroup, Label, Input } from "reactstrap";

class AccessForm extends React.Component {
  state = { cc: null, status: null };

  submit = async e => {
    e.preventDefault();
    try {
      let client = await this.props.create(this.state.cc);
      if (this.props.onSubmitted) this.props.onSubmitted(client);
      this.setState({ status: null });
    } catch (e) {
      this.setState({ status: e.message });
    }
  };

  change = ({ target }) => {
    this.setState({ cc: target.value });
  };

  render() {
    return (
      <Form onSubmit={this.submit}>
        <FormGroup className="text-center">
          <Label>
            digite su numero de cedula de ciudadania o tarjeta de identidad
          </Label>
          <Input
            type="text"
            pattern="^[\d]*$"
            required
            className="text-center"
            onChange={this.change}
          />
        </FormGroup>
        <Button type="submit" color="primary" className="m-auto d-block">
          REGISTRARSE
        </Button>
        {this.state.status && (
          <Alert color="danger" className="mt-2 text-center text-uppercase">
            {this.state.status}
          </Alert>
        )}
      </Form>
    );
  }
}

const mapDispatch = dispatch => ({
  create: cc => dispatch(create(cc))
});

export default connect(
  null,
  mapDispatch
)(AccessForm);
