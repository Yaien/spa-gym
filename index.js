const { app, BrowserWindow } = require("electron");
const { join } = require("path");
const {
  default: install,
  REACT_DEVELOPER_TOOLS,
  REDUX_DEVTOOLS
} = require("electron-devtools-installer");

let win;

async function dev() {
  await install(REACT_DEVELOPER_TOOLS);
  await install(REDUX_DEVTOOLS);
}

function create() {
  win = new BrowserWindow({
    title: "Code Inventario",
    minWidth: 800,
    minHeight: 600,
    width: 1024,
    height: 788,
    autoHideMenuBar: true
  });
  win.on("closed", () => (win = null));
  win.loadFile(join(__dirname, "public/index.html"));
  return dev();
}

app.on("ready", create);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") app.quit();
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) create();
});
